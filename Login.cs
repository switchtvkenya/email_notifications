﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmailNotif
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtPassword.Text = "";
            txtUserName.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (txtUserName.Text == "admin" && txtPassword.Text == "p@$$123")
            {
                // login successful               
                txtPassword.Text = "";
                txtUserName.Text = "";

                Dashboard fm = new Dashboard(this);
                fm.Show();

                // this.Close();
                this.Hide();

            }
            else
            {
                MessageBox.Show("Invalid user details", "Login");
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {
            CenterToScreen();
        }
    }
}
