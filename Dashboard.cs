﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Net;

namespace EmailNotif
{
    public partial class Dashboard : Form
    {
        DataClasses1DataContext db = new DataClasses1DataContext(getConnectionString());
        private Int32 Id = 0, counter = 0, total = 0;
        private string sNewsLetterTitle = "", strHTML = "";
        Timer timer = new Timer();

        Login login_local;

        public Dashboard(Login login)
        {
            login_local = login;

            InitializeComponent();

            timer1.Tick += new EventHandler(timer1_Tick); // Everytime timer ticks, timer_Tick will be called
            timer1.Interval = (1500) * (300);             // Timer will tick every 5 mins of inactivity
            timer1.Enabled = true;                       // Enable the timer
            timer1.Start();                              // Start the time
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CenterToScreen();

          //  timer1.Enabled = true;

            var aNL = from p in db.Newsletters
                      select new
                      {
                          nl_Title = p.nl_Title,
                          nl_ID = p.nl_ID
                      };

            comboBox1.ValueMember = "nl_ID";
            comboBox1.DisplayMember = "nl_Title";
            comboBox1.DataSource = aNL;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // SendRequestforApprovalMail();
            //  SendRequestforApprovalMail_();
              Id = (int)comboBox1.SelectedValue;
              progressBar1.Maximum = 100;
              progressBar1.Step = 1;
              progressBar1.Value = 0;
              progressBar1.Visible = true;
              button1.Enabled = false;
              backgroundWorker1.WorkerReportsProgress = true;
              backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // TODO: do something with final calculation.
            lblCount.Text = counter.ToString() + " email(s) sent";
            button1.Enabled = true;
            progressBar1.Visible = false;
        }

        protected void SendRequestforApprovalMail_()
        {
            int Id = 3;
            string sNewsLetterTitle = "Switch Newsletter";
            string strHTML = "<table width=\"530\" cellpadding=\"0\" cellspacing=\"0\" border=0>";
            strHTML = strHTML + "<tr><td height=45 style=\"background-color:#99177E;text-align:center;color:#FFFFFF\"><a href=\"https://www.switchtv.ke/adm/view/nl.aspx?id=" + Id + "\" style=\"color:#ffFFFF;text-decoration:none;\" target =\"blank\">View this email in your browser</a></td></tr>";

            try
            {
                MailMessage mail = new MailMessage();
              //  SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                SmtpClient SmtpServer = new SmtpClient("smtp.office365.com");

                //   mail.From = new MailAddress("switchtvkenya@gmail.com", "Switch TV Marketing");
                mail.From = new MailAddress("no-reply@switchmedia.ke", "Switch TV Marketing");
                mail.To.Add("jerrydindi@gmail.com");
                mail.Subject = sNewsLetterTitle;

                mail.IsBodyHtml = true;
                string htmlBody;

                htmlBody = "<html><body>";
                htmlBody += strHTML;
                htmlBody += "</body></html>";

                mail.Body = htmlBody;

                SmtpServer.Port = 587;
                //SmtpServer.Credentials = new System.Net.NetworkCredential("switchtvkenya@gmail.com", "Zw!tch.1st"); 
                SmtpServer.Credentials = new System.Net.NetworkCredential("no-reply@switchmedia.ke", "Switch.123");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                //   db.sp_MarkEmailAddress(pc.nlc_ID);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        protected void fetchArtice()
        {
            sNewsLetterTitle = "";

            var oNL = from p in db.Newsletters
                      where p.nl_ID == Id
                      select new
                      {
                          nl_Title = p.nl_Title,
                      };
            foreach (var pc in oNL)
            {
                sNewsLetterTitle = pc.nl_Title;
            }

            var oArticle = from p in db.Newsletter_Stories
                           where p.nls_Newsletter == Id
                           select new
                           {
                               nls_Title = p.nls_Title,
                               nls_Body = p.nls_Body,
                               nls_Image = p.nls_Image,
                               nls_TVShow = p.nls_TVShow,
                               nls_type = p.nls_Type
                           };
            foreach (var pc in oArticle)
            {
                switch (pc.nls_type)
                {
                    case 1: // Image Top                            
                        strHTML = strHTML + "<!-- Hero Image -->" +
                                "							<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                                "								<tr>" +
                                "									<td class=\"fluid-img\" style=\"font-size:0pt; line-height:0pt; text-align:left;\">" +
                                "                                     <img src=\"https://www.switchtv.ke/Attachments/" + pc.nls_Image + "\" border=\"0\" width=\"650\" height=\"366\" alt=\"\" />" +
                                "                                   </td>" +
                                "								</tr>" +
                                "							</table>" +
                                "							<!-- END Hero Image -->" +
                                "							<!-- Intro -->" +
                                "							<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#ffffff\">" +
                                "								<tr>" +
                                "									<td style=\"padding-bottom: 10px;\">" +
                                "										<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                                "											<tr>" +
                                "												<td class=\"p30-15\" style=\"padding: 60px 30px;\">" +
                                "													<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                                "														<tr>" +
                                "															<td class=\"h1 pb25\" style=\"color:#444444; font-family:Arial,sans-serif; font-size:32px; line-height:42px; ; padding-bottom:25px;\">" + pc.nls_Title + "</td>" +
                                "														</tr>" +
                                "														<tr>" +
                                "															<td class=\"text-center pb25\" style=\"color:#666666; font-family:Arial,sans-serif; font-size:16px; line-height:30px;  padding-bottom:25px;\">" + pc.nls_Body + "</td>" +
                                "														</tr>" +
                                "													</table>" +
                                "												</td>" +
                                "											</tr>" +
                                "										</table>" +
                                "									</td>" +
                                "								</tr>" +
                                "							</table>" +
                                "							<!-- END Intro -->";
                        break;
                    case 2: // Image Right
                        strHTML = strHTML + "<!-- Article / Purple Background -->" +
                            "							<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                            "								<tr>" +
                            "									<td bgcolor=\"#9c3cd3\">" +
                            "										<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                            "											<tr>" +
                            "												<th class=\"column-top\" dir=\"ltr\" width=\"217\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;\">" +
                            "													<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                            "														<tr>" +
                            "															<td class=\"fluid-img\" style=\"font-size:0pt; line-height:0pt; text-align:left;\"><img src=\"https://www.switchtv.ke/Attachments/" + pc.nls_Image + "\" width=\"217\" height=\"440\" border=\"0\" alt=\"\" /></td>" +
                            "														</tr>" +
                            "													</table>" +
                            "												</th>" +
                            "												<th class=\"column\" width=\"50\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;\"></th>" +
                            "												<th class=\"column\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;\">" +
                            "													<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                            "														<tr>" +
                            "															<td class=\"p30-15\">" +
                            "																<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                            "																	<tr>" +
                            "																		<td class=\"h3 white pb20\" style=\"font-family:Arial,sans-serif; font-size:25px; line-height:32px; text-align:left; color:#ffffff; padding-bottom:20px;\">" + pc.nls_Title + "</td>" +
                            "																	</tr>" +
                            "																	<tr>" +
                            "																		<td class=\"text white pb20\" style=\"font-family:Arial,sans-serif; font-size:14px; line-height:26px; text-align:left; color:#ffffff; padding-bottom:20px;\">" + pc.nls_Body + "</td>" +
                            "																	</tr>" +
                            "																</table>" +
                            "															</td>" +
                            "														</tr>" +
                            "													</table>" +
                            "												</th>" +
                            "												<th class=\"column\" width=\"50\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;\"></th>" +
                            "											</tr>" +
                            "										</table>" +
                            "									</td>" +
                            "								</tr>" +
                            "							</table>" +
                            "							<!-- END Article / Purple Background -->";
                        break;
                    case 3: // Image Left
                        strHTML = strHTML + "<!-- Article Secondary / White Background -->" +
                            "							<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                            "								<tr>" +
                            "									<td bgcolor=\"#ffffff\">" +
                            "										<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" dir=\"rtl\" style=\"direction: rtl;\">" +
                            "											<tr>" +
                            "												<th class=\"column-dir-top\" dir=\"ltr\" width=\"217\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr; vertical-align:top;\">" +
                            "													<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                            "														<tr>" +
                            "															<td class=\"fluid-img\" style=\"font-size:0pt; line-height:0pt; text-align:left;\"><img src=\"https://www.switchtv.ke/Attachments/" + pc.nls_Image + "\" width=\"217\" height=\"440\" border=\"0\" alt=\"\" /></td>" +
                            "														</tr>" +
                            "													</table>" +
                            "												</th>" +
                            "												<th class=\"column-dir\" dir=\"ltr\" width=\"50\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;\"></th>" +
                            "												<th class=\"column-dir\" dir=\"ltr\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;\">" +
                            "													<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                            "														<tr>" +
                            "															<td class=\"p30-15\">" +
                            "																<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                            "																	<tr>" +
                            "																		<td class=\"h3 pb20\" style=\"color:#000000; font-family:Arial,sans-serif; font-size:25px; line-height:32px; text-align:left; padding-top: 20px; padding-bottom:20px;\">" + pc.nls_Title + "</td>" +
                            "																	</tr>" +
                            "																	<tr>" +
                            "																		<td class=\"text pb20\" style=\"color:#666666; font-family:Arial,sans-serif; font-size:14px; line-height:26px; text-align:left; padding-bottom:20px;\">" + pc.nls_Body + "</td>" +
                            "																	</tr>" +
                            "																</table>" +
                            "															</td>" +
                            "														</tr>" +
                            "													</table>" +
                            "												</th>" +
                            "												<th class=\"column\" width=\"50\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;\"></th>" +
                            "											</tr>" +
                            "										</table>" +
                            "									</td>" +
                            "								</tr>" +
                            "							</table>" +
                            "							<!-- END Article Secondary / White Background -->";
                        break;
                    case 4: // No image | iframe for video
                        strHTML = strHTML + "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#ffffff\">" +
                            "								<tr>" +
                            "									<td style=\"padding-bottom: 10px;\">" +
                            "										<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                            "											<tr>" +
                            "												<td class=\"p30-15\" style=\"padding: 60px 30px;\">" +
                            "													<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                            "														<tr>" +
                            "															<td class=\"h1 pb25\" style=\"color:#444444; font-family:Arial,sans-serif; font-size:32px; line-height:42px; ; padding-bottom:25px;\">" + pc.nls_Title + "</td>" +
                            "														</tr>" +
                            "														<tr>" +
                            "															<td class=\"text-center pb25\" style=\"color:#666666; font-family:Arial,sans-serif; font-size:16px; line-height:30px;  padding-bottom:25px;\">" + pc.nls_Body + "</td>" +
                            "														</tr>" +
                            "													</table>" +
                            "												</td>" +
                            "											</tr>" +
                            "										</table>" +
                            "									</td>" +
                            "								</tr>" +
                            "							</table>" +
                            "							<!-- END Intro -->";
                        break;
                    default:
                        // code block
                        break;
                }
            }
        }

        private void backgroundWorker1_DoWork_1(object sender, DoWorkEventArgs e)
        {
            //button1.Enabled = false;
            //progressBar1.Visible = true;
            fetchArtice();
          //  resetEmailsToSend();
            var backgroundWorker = sender as BackgroundWorker;
            var oEmailAddresses = from p in db.Newsletter_Emails
                                  where p.nlc_isSent == false && p.nlc_Subscribed == true orderby p.nlc_ID descending
                                  select new
                                  {
                                      nlc_Email = p.nlc_Email,
                                      nlc_ID = p.nlc_ID
                                  };

            total = oEmailAddresses.Count();
            foreach (var pc in oEmailAddresses)
            {
                counter += 1;
                sendEmail(pc.nlc_Email, sNewsLetterTitle, strHTML, pc.nlc_ID);
                backgroundWorker.ReportProgress((counter * 100) / total);
            }
        }

        private void resetEmailsToSend()
        {
            (from p in db.Newsletter_Emails where p.nlc_Subscribed == true select p).ToList().ForEach(x => x.nlc_isSent = false);

            db.SubmitChanges();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            // re-initialize timer
            timer1.Stop();
            timer1.Start();

            // Initializes the variables to pass to the MessageBox.Show method.
            string message = "Are you sure you want to logout?";
            string caption = "Logout";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            // Displays the MessageBox.
            result = MessageBox.Show(message, caption, buttons);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                // Closes the parent form.
                this.Close();

                // back to login
                //Login fm = new Login();
                //fm.Show();

                login_local.Visible = true;

            }
        }


        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            timer1.Enabled = false;

            // Closes the parent form.
            this.Close();

            // back to login
            login_local.Visible = true;

        }

        private void sendEmail(string recepient, string subject, string email, int recepID) {             
            
                try
                {
                    // re-initialize timer
                    timer1.Stop();
                    timer1.Start();

                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient("smtp.office365.com");

                //   mail.From = new MailAddress("marketing@switchmedia.ke", "Switch TV Marketing");
                mail.From = new MailAddress("no-reply@switchmedia.ke", "Switch TV Marketing");
                mail.To.Add(recepient);
                  //  mail.Bcc.Add(recepient);
                mail.Subject = subject;

                mail.IsBodyHtml = true;
                string htmlBody;

                htmlBody = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\">" +
                    "<head>" +
                    "	<!--[if gte mso 9]>" +
                    "	<xml>" +
                    "		<o:OfficeDocumentSettings>" +
                    "		<o:AllowPNG/>" +
                    "		<o:PixelsPerInch>96</o:PixelsPerInch>" +
                    "		</o:OfficeDocumentSettings>" +
                    "	</xml>" +
                    "	<![endif]-->" +
                    "	<meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\" />" +
                    "	<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\" />" +
                    "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />	<meta name=\"format-detection\" content=\"date=no\" />" +
                    "	<meta name=\"format-detection\" content=\"address=no\" />	<meta name=\"format-detection\" content=\"telephone=no\" />" +
                    "	<meta name=\"x-apple-disable-message-reformatting\" />    <!--[if !mso]><!-->	<!-- <link href=\"https://fonts.googleapis.com/css?family=Open Sans:400,400i,700,700i\" rel=\"stylesheet\" /> -->" +
                    "	<link href=\"https://fonts.googleapis.com/css2?family=Open+Sans:wght@700&display=swap\" rel=\"stylesheet\">" +
                    "    <!--<![endif]-->" +
                    "	<title>Email Template</title>" +
                    "	<!--[if gte mso 9]>	<style type=\"text/css\" media=\"all\">" +
                    "		sup { font-size: 100% !important; }" +
                    "	</style>" +
                    "	<![endif]-->" +
                    "		<style type=\"text/css\" media=\"screen\">" +
                    "		/* Linked Styles */" +
                    "		body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#2AD2C9; -webkit-text-size-adjust:none }" +
                    "		a { color:#000001; text-decoration:none }" +
                    "		p { padding:0 !important; margin:0 !important }" +
                    " 		img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }" +
                    "		.mcnPreviewText { display: none !important; }" +
                    "						/* Mobile styles */" +
                    "		@media only screen and (max-device-width: 480px), only screen and (max-width: 480px)" +
                    " {			.mobile-shell { width: 100% !important; min-width: 100% !important; }" +
                    "			.bg { background-size: 100% auto !important; -webkit-background-size: 100% auto !important; }" +
                    "						.text-header,			.m-center { text-align: center !important; }" +
                    "						.center { margin: 0 auto !important; }" +
                    "			.container { padding: 20px 10px !important }" +
                    "						.td { width: 100% !important; min-width: 100% !important; }" +
                    "			.m-br-15 { height: 15px !important; }" +
                    "			.p30-15 { padding: 30px 15px !important; }" +
                    "			.p0-15-30 { padding: 0px 15px 30px 15px !important; }" +
                    "			.mpb30 { padding-bottom: 30px !important; }" +
                    "			.m-td," +
                    "			.m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }" +
                    "			.m-block { display: block !important; }" +
                    "			.fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }" +
                    "			.column," +
                    "			.column-dir," +
                    "			.column-top," +
                    "			.column-empty," +
                    "			.column-empty2," +
                    "			.column-dir-top { float: left !important; width: 100% !important; display: block !important; }" +
                    "			.column-empty { padding-bottom: 30px !important; }" +
                    "			.column-empty2 { padding-bottom: 10px !important; }" +
                    "			.content-spacing { width: 15px !important; }" +
                    "		}" +
                    "	</style>" +
                    "</head>" +
                    "<body class=\"body\" style=\"padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#2AD2C9; -webkit-text-size-adjust:none;\">	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#2AD2C9\">" +
                    "		<tr>" +
                    "			<td align=\"center\" valign=\"top\">" +
                    "				<table width=\"650\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"mobile-shell\">" +
                    "					<tr>" +
                    "						<td class=\"td container\" style=\"width:650px; min-width:650px; font-size:0pt; line-height:0pt; margin:0; font-weight:normal; padding:55px 0px;\">" +
                    "							<!-- Header -->" +
                    "							<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                    "								<tr>" +
                    "									<td class=\"p30-15 tbrr\" style=\"padding: 30px; border-radius:12px 12px 0px 0px;\" bgcolor=\"#ffffff\">" +
                    "										<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                    "											<tr>" +
                    "												<th class=\"column-top\" width=\"145\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;\">" +
                    "													<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                    "														<tr>" +
                    "															<td class=\"img m-center\" style=\"font-size:0pt; line-height:0pt; text-align:left;\"><img src=\"https://www.switchtv.ke/img/nl/logo.png\" height=\"90\" border=\"0\" alt=\"\" /></td>" +
                    "														</tr>" +
                    "													</table>" +
                    "												</th>" +
                    "												<th class=\"column-empty2\" width=\"1\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;\"></th>" +
                    "												<th class=\"column\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;\">" +
                    "													<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                    "														<tr>" +
                    "															<td class=\"text-header\" style=\"color:#999999; font-family:Arial,sans-serif; font-size:13px; line-height:18px; text-align:right;\"><a href=\"https://www.switchtv.ke/adm/view/nl.aspx?id=" + Id + "\" target=\"_blank\" class=\"link2\" style=\"color:#999999; text-decoration:none;\"><span class=\"link2\" style=\"color:#999999; text-decoration:none;\">Open in your browser</span></a></td>" +
                    "														</tr>" +
                    "													</table>" +
                    "												</th>" +
                    "											</tr>" +
                    "										</table>" +
                    "									</td>" +
                    "								</tr>" +
                    "							</table>" +
                    "							<!-- END Header -->";

                htmlBody += email;

                htmlBody += "<!-- CTA -->" +
                    "							<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                    "								<tr>" +
                    "									<td style=\"padding: 70px 30px 70px 30px;\" class=\"p30-15\" bgcolor=\"#99177e\">" +
                    "										<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                    "											<tr>" +
                    "												<td class=\"h2 pb30\" style=\"color:#ffffff; font-family:Arial,sans-serif; font-size:30px; line-height:36px; text-align:center; padding-bottom:30px;\">Watch Switch TV on Your Phone</td>" +
                    "											</tr>" +
                    "											<tr>" +
                    "												<td class=\"text-center2 pb30\" style=\"color:#ffffff; font-family:Arial,sans-serif; font-size:16px; line-height:30px; text-align:center; padding-bottom:30px;\">Keep watching your favourite shows. Available Anywhere, Anytime.</td>" +
                    "											</tr>" +
                    "											<!-- Button -->" +
                    "											<tr>" +
                    "												<td align=\"center\">" +
                    "													<table class=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"text-align:center;\">" +
                    "														<tr>" +
                    "															<td class=\"text-button blue-button\" style=\"color:#444444; font-family:Arial,sans-serif; font-size:14px; line-height:18px; text-align:center; border-radius:10px; text-transform:uppercase; background:#FF8200; padding:12px 30px;\"><a href=\"https://play.google.com/store/apps/details?id=ke.switchmedia.switchtv\" target=\"_blank\" class=\"link-yellow\" style=\"color:#ffffff; text-decoration:none;\"><span class=\"link-yellow\" style=\"color:#ffffff; text-decoration:none;\">Donwload App</span></a></td>" +
                    "														</tr>" +
                    "													</table>" +
                    "												</td>" +
                    "											</tr>" +
                    "											<!-- END Button -->" +
                    "										</table>" +
                    "									</td>" +
                    "								</tr>" +
                    "							</table>" +
                    "							<!-- END CTA -->" +
                    "														<!-- Banner -->" +
                    "							<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                    "								<tr>" +
                    "									<td class=\"fluid-img\" style=\"font-size:0pt; line-height:0pt; text-align:left;\"><img src=\"https://www.switchtv.ke/img/nl/mobile.png\" width=\"650\" height=\"80\" border=\"0\" alt=\"\" /></td>" +
                    "								</tr>" +
                    "							</table>" +
                    "							<!-- END Banner -->" +
                    "							<!-- Footer -->" +
                    "							<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                    "								<tr>" +
                    "									<td class=\"p30-15 bbrr\" style=\"padding: 50px 30px; border-radius:0px 0px 12px 12px;\" bgcolor=\"#ffffff\">" +
                    "										<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                    "											<tr>" +
                    "												<td align=\"center\" style=\"padding-bottom: 30px;\">" +
                    "													<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                    "														<tr>" +
                    "															<td class=\"img\" width=\"55\" style=\"font-size:0pt; line-height:0pt; text-align:left;\"><a href=\"https://facebook.com/switchtvkenya\" target=\"_blank\"><img src=\"https://www.switchtv.ke/img/nl/t2_facebook.jpg\" width=\"34\" height=\"34\" border=\"0\" alt=\"\" /></a></td>" +
                    "															<td class=\"img\" width=\"55\" style=\"font-size:0pt; line-height:0pt; text-align:left;\"><a href=\"https://twitter.com/switchtvkenya\" target=\"_blank\"><img src=\"https://www.switchtv.ke/img/nl/t2_twitter.jpg\" width=\"34\" height=\"34\" border=\"0\" alt=\"\" /></a></td>" +
                    "															<td class=\"img\" width=\"55\" style=\"font-size:0pt; line-height:0pt; text-align:left;\"><a href=\"https://instagram.com/switchtvke\" target=\"_blank\"><img src=\"https://www.switchtv.ke/img/nl/t2_instagram.jpg\" width=\"34\" height=\"34\" border=\"0\" alt=\"\" /></a></td>" +
                    "															<td class=\"img\" width=\"34\" style=\"font-size:0pt; line-height:0pt; text-align:left;\"><a href=\"https://www.linkedin.com/company/switchmediakenya/\" target=\"_blank\"><img src=\"https://www.switchtv.ke/img/nl/t2_linkedin.jpg\" width=\"34\" height=\"34\" border=\"0\" alt=\"\" /></a></td>" +
                    "														</tr>" +
                    "													</table>" +
                    "												</td>" +
                    "											</tr>" +
                    "											<tr>" +
                    "												<td class=\"text-footer1 pb10\" style=\"color:#999999; font-family:Arial,sans-serif; font-size:14px; line-height:20px; text-align:center; padding-bottom:10px;\">Switch TV Kenya</td>" +
                    "											</tr>" +
                    "											<tr>" +
                    "												<td class=\"text-footer2\" style=\"color:#999999; font-family:Arial,sans-serif; font-size:12px; line-height:26px; text-align:center;\">P.O. Box. 40712 - 00100, Red Cross Road South C, Kenya</td>" +
                    "											</tr>" +
                    "										</table>" +
                    "									</td>" +
                    "								</tr>" +
                    "								<tr>" +
                    "									<td class=\"text-footer3\" style=\"padding: 40px 15px 0px; color:#425f8e; font-family:Arial,sans-serif; font-size:12px; line-height:26px; text-align:center;\">" +
                    "										<a href=\"https://www.switchtv.ke/adm/edit/nlu.aspx?id=" + Id + "&uid=" + recepID + "\" target=\"_blank\" class=\"link-blue-u\" style=\"color:#425f8e; text-decoration:underline;\"><span class=\"link-blue-u\" style=\"color:#425f8e; text-decoration:underline;\">Unsubscribe</span></a> from this mailing list." +
                    "									</td>" +
                    "								</tr>" +
                    "							</table>" +
                    "							<!-- END Footer -->" +
                    "						</td>" +
                    "					</tr>" +
                    "				</table>" +
                    "			</td>" +
                    "		</tr>" +
                    "	</table>" +
                    " </body>" +
                    "</html>";

                mail.Body = htmlBody;

                SmtpServer.Port = 587;
                //SmtpServer.Credentials = new System.Net.NetworkCredential("marketing@switchmedia.ke", "5witch.123");
                SmtpServer.Credentials = new System.Net.NetworkCredential("no-reply@switchmedia.ke", "Switch.123");
                SmtpServer.EnableSsl = true;
               // ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;

                SmtpServer.Send(mail);
                db.sp_MarkEmailAddress(recepID);
                  //  counter += 1;
                  //  lblCount.Text = "Emails Sent: " + counter.ToString();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.InnerException.ToString());
                }
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        public static String getConnectionString()
        {
            String connectionString;
            connectionString = ConfigurationManager.ConnectionStrings["DBConnSwitchTV"].ConnectionString;
            return connectionString;
        }
    }

}
